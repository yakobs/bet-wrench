const mongoose = require('mongoose');
const schema = mongoose.Schema;

var competitionSchema = new schema({
	name: String,
	country: String,
	level: Number,
});

const Competition = mongoose.model("Competition", competitionSchema);
module.exports = Competition;