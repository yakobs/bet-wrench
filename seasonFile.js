const fs = require('fs');

console.log("Starting seasonFile");

var addSeason = (season) => {
	console.log('Adding season', season);
};

var addAllSeasons = () => {
	console.log('Adding all seasons');
	var path = './seasons/';
	fs.readdir(path, function(err, items) {
		for(var i=0; i<items.length; i++) {
			split_item = items[i].slice(0, -4).replace('-',' ').split('_');
			console.log(split_item);
		}
	});
};

var removeSeason = (season) => {
	console.log('Removing season', season);
};

var removeAllSeasons = () => {
	console.log('Removing all seasons');
};

module.exports = {
	addSeason,
	addAllSeasons,
	removeSeason,
	removeAllSeasons
};