const fs = require('fs');
const mongoose = require('mongoose');
const yargs = require('yargs');

const seasonFile = require('./seasonFile.js');
const competition = require('./models/competition.js');
const season = require('./models/season.js');
const team = require('./models/team.js');
const match = require('./models/match.js');

//Connecting to database
mongoose.connect('mongodb://localhost:27017/dbfootball', function() {
	console.log('Connection has been made.');
})
.catch(err => {
	console.error('App starting error:', err.stack);
	process.exit(1);
});

// Setting command line actions
const argv = yargs
	.command('add', 'Add a new season to the database.', {
		season: {
			describe: 'Season file name in .csv format.',
			demand: true,
			alias: 's'
		}
	})
	.command('addAll', 'Add all seasons found in the seasons folder of the application.')
	.command('remove', 'Remove a season from the database.', {
		season: {
			describe: 'Season id.',
			demand: true,
			alias: 's'
		}
	})
	.command('removeAll', 'Remove all data from database.')
	.help()
	.argv;
var command = argv._[0];

if (command === 'add') {
	seasonFile.addSeason(argv.season);
} else if (command === 'addAll') {
	seasonFile.addAllSeasons();
} else if (command === 'remove') {
	seasonFile.removeSeason(argv.season);
} else if (command === 'removeAll') {
	seasonFile.removeAllSeasons();
} else {
	console.log('Command not recognized');
};
